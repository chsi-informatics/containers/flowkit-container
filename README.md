# flowkit-container

## Overview

This repository contains the Singularity definition for building a container containing several Python libraries for flow cytometry analysis, including FlowKit, scikit-learn, jupyter, and other libraries for dimension reduction and clustering. See the flowkit-singularity.def file for a complete listing of included libraries.

The definition file builds directly from the official Ubuntu 24.04 (Noble) Docker image.

## Build Container

The container is built automatically via CI/CD in two ways: 
* When the def file is modified - this is the 'latest' build
* When the repository is tagged - these are official releases

If the CI build is successful, the resulting .sif file is automatically pushed to the container registry. Both these CI steps are defined in the `.gitlab-ci.yml` file included in this repository.

### Tagged Releases

Tagged releases use the following scheme. The main version number tracks the version of the FlowKit package. The tag's suffix is the container revision number. For example the tag `v1.2.1r1` corresponds the first revision of the built container containing version 1.2.1 of FlowKit.

### Building the Container Locally

To build the Singularity container locally, use the following command:

`sudo singularity build flowkit-container.sif flowkit-singularity.def`

## Deploy to DCC OnDemand

 For the container to be available on the Duke Compute Cluster's OnDemand service, the .sif file must be added to the `/opt/apps/communtity/` directory or subdirectory. To pull the latest version of the built container from the registry use the following command from a terminal on the DCC.

Pull the latest dev version:

```
wget --directory-prefix /opt/apps/community/od_chsi_jupyter --no-check-certificate "https://research-containers-01.oit.duke.edu/chsi-informatics/flowkit-container_latest.sif"
```

Pull a tagged version:

```
TAG=""  # Define desired tag/version, e.g. "v1.2.1r1"

wget --directory-prefix /opt/apps/community/od_chsi_jupyter --no-check-certificate "https://research-containers-01.oit.duke.edu/chsi-informatics/flowkit-container_${TAG}.sif"
```

## Running

The primary method of running the container is [Open OnDemand](https://dcc-ondemand-01.oit.duke.edu/) via the Jupyter Singularity community app (Apptainer).

### Run locally

To run the container from a local SIF file:

`sudo singularity run -B $PWD:/opt/notebooks flowkit-container.sif`
